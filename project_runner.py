from pyparsing import col
from tensorflow import keras
from keras import optimizers
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint
import math
import numpy as np
import matplotlib.pyplot as plt
from keras.utils.np_utils import to_categorical
from sklearn.model_selection import train_test_split
import tensorflow_addons as tfa

#Commented code below is for one time preprocessing
'''
#Append the directory to your python path using sys
import sys
import os
sys_path = 'C:\\Users\\wtmul\\Documents\\'
sys.path.append(sys_path)
#print(sys.path)

expert_labels = os.path.join(sys_path, 'expert.json')
student_labels = os.path.join(sys_path, 'student.json')
image_path = os.path.join(sys_path, 'Radiographs\\')
mask_path = os.path.join(sys_path, 'masks\\')
print('Path to data: {}'.format(expert_labels))

import json

#classifications = [None] * 1051

def format_json(json_list):
  for elem in json_list:
    idx = elem['External ID'].find('.')
    elem['External ID'] = int(elem['External ID'][:idx])
  return json_list

def load_json(filename):
    with open(filename) as jsonfile:
        unsorted = json.load(jsonfile)
        unsorted = format_json(unsorted)
        now_sorted = sorted(unsorted, key=lambda i: i['External ID'])
        nparray = np.empty(num_images)
        for i in range(num_images):
          temp = now_sorted[i]['Label']['objects'][0]['title']
          if (temp == 'None'):
            #classifications[now_sorted[i]['External ID'] - 1] = 0
            nparray[i] = 0
          else:
            #classifications[now_sorted[i]['External ID'] - 1] = 1
            nparray[i] = 1
        return nparray

from PIL import Image, ImageOps

def load_images(path):
  images = np.empty((num_images, image_height, image_width))
  black = Image.new(mode="L", size=(image_width, image_height))
  idx = 0
  num = 1
  while(idx < num_images):
    try:
      image = Image.open(image_path + str(num) + ".JPG")
    except:
      num += 1
      continue
    mask = Image.open(mask_path + str(num) + ".JPG")
    image_gray = ImageOps.grayscale(image)
    image.close()
    image_masked = Image.composite(image_gray, black, mask)
    mask.close()
    image_gray.close()
    #if (classifications[num - 1] == 0):
      #image_masked.save(".\\Images\\Normal\\" + str(num) + ".JPG")
    #else:
      #image_masked.save(".\\Images\\Abnormal\\" + str(num) + ".JPG")
    image_array = np.asarray(image_masked)
    image_masked.close()
    images[idx] = image_array
    idx += 1
    num += 1
  images = np.expand_dims(images, axis=3)
  return images

data_labels = load_json(expert_labels)
data_images = load_images(image_path)

data_images /= 255

x_train, x_test, y_train, y_test = train_test_split(data_images, data_labels, test_size=test_ratio, random_state=rand_state)
'''
#Parameters/Constants/Hyperparameters
num_images = 1000
test_ratio = 0.15
num_classes = 2
class_weight = {0:2.5, 1:1}
rand_state = 420
image_height = 840
image_width = 1615
batch_size = 8
epochs = 10
num_filters = 8
filter_size = (3, 3) #lowering reduces params
pool_size = (2, 2) #raising reduces params

train_generator = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        rotation_range=5,
        horizontal_flip=True,
        validation_split=test_ratio)

val_generator = ImageDataGenerator(rescale=1./255, validation_split=test_ratio)

train_generator = train_generator.flow_from_directory(
        '.\\Images',  # this is the target directory
        target_size=(image_height, image_width),
        batch_size=batch_size,
        class_mode='categorical',
        seed=rand_state,
        subset="training",
        color_mode='grayscale')

validation_generator = val_generator.flow_from_directory(
        '.\\Images',
        target_size=(image_height, image_width),
        batch_size=batch_size,
        class_mode='categorical',
        seed=rand_state,
        subset="validation",
        color_mode='grayscale')

def base_cnn():
  """
  Define a convolutional neural network using the Sequential model. This is the 
  basic CNN that you will need to reuse for the remaining parts of the assignment.
  It would be good to familiarize yourself with the workings of this basic CNN.
  """
  model = Sequential()

  model.add(Conv2D(num_filters, filter_size, padding='same',input_shape=(image_height, image_width, 1)))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(pool_size=pool_size))
  model.add(Dropout(0.25))

  model.add(Conv2D(2 * num_filters, filter_size, padding='same'))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(pool_size=pool_size))
  model.add(Dropout(0.25))

  model.add(Conv2D(3 * num_filters, filter_size, padding='same'))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(pool_size=pool_size))
  model.add(Dropout(0.25))

  model.add(MaxPooling2D(pool_size=pool_size))
  model.add(MaxPooling2D(pool_size=pool_size))

  model.add(Flatten())
  model.add(Dense(256)) #lowering reduces params
  model.add(Activation('relu'))
  model.add(Dropout(0.5))

  model.add(Dense(num_classes))
  model.add(Activation('softmax'))

  opt = keras.optimizers.RMSprop(learning_rate=0.00001, decay=1e-6)
  #opt = tfa.optimizers.AdamW(learning_rate=0.001, weight_decay=0.0001)

  model.compile(loss='categorical_crossentropy',
                optimizer=opt,
                metrics=['accuracy'])
  print(model.summary())

  return model

# create a callback that will save the best model while training
save_best_model = ModelCheckpoint('best_model.relu', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
model = base_cnn()
history_activation = model.fit(train_generator,
                                            steps_per_epoch=850 // batch_size,
                                            epochs=epochs,
                                            validation_data=validation_generator,
                                            validation_steps=150 // batch_size,
                                            callbacks=[save_best_model],
                                            class_weight=class_weight)
# Plot training accuracy
plt.plot(history_activation.history['accuracy'], 'o-', label='CNN + relu activation')

plt.title('training accuracy')
plt.ylabel('training accuracy')
plt.xlabel('epoch')
plt.legend(loc='lower right')
plt.show()

# Plot validation accuracy
plt.plot(history_activation.history['val_accuracy'], 'o-', label='CNN + relu activation')
plt.title('validation accuracy')
plt.ylabel('validation accuracy')
plt.xlabel('epoch')
plt.legend(loc='lower right')
plt.show()
